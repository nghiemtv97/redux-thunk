import {
  FETCH_POST_ERROR,
  FETCH_POST_REQUEST,
  FETCH_POST_SUCCESS,
} from "../constants/postConstants";

const initalState = {
  requesting: false,
  success: false,
  message: null,
  data: null,
};

function postReducers(state = initalState, payload) {
  switch (payload.type) {
    case FETCH_POST_REQUEST:
      return {
        ...state,
        requesting: true,
      };
    case FETCH_POST_SUCCESS:
      return {
        ...state,
        requesting: false,
        success: true,
        data: payload.data,
      };
    case FETCH_POST_ERROR:
      return {
        ...state,
        requesting: false,
        success: false,
        message: payload.message,
      };
    default:
      return state;
  }
}

export default postReducers;
