import { combineReducers } from "redux";
import postReducer from './postReducer';
import commentReducers from "./commentReducer";

const reducers = combineReducers({
    posts: postReducer,
    comment:commentReducers
});

export default (state, action) => reducers(state, action);