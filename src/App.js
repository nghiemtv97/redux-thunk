import "./App.css";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadPost } from "./redux/action/postAction";
import { loadComment } from "./redux/action/commentAction";


function App() {
  const data = useSelector((state) => state.posts.data);
  const dataComment = useSelector(state => state.comment.data)
  const requesting = useSelector((state) => state.posts.requesting);
  console.log(requesting, "requesting");
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(loadPost(), loadComment());
  }, []);
  return <div className="App">

    {
      requesting ? "nghiem" : (data && data.length > 0) ? <div>
        {console.log(data, "xxxxxxxxxxx")}
        {data.map(item => <li key={item.id}>{item.title}</li>)}
      </div> : null
    }
    {

      requesting ? "nghiem" : (dataComment && dataComment.length > 0) ? <div>
        {dataComment.map(item => <li key={item.id}>{item.body}</li>)}
      </div> : null
    }
  </div>;
}

export default App;
